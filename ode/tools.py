#!/usr/bin/env python
#
# abstract ODE Tool interface
#

import os
import re
import sys
import json
import time
import psycopg2
import subprocess 

# //////////////////////////////////////////////////////

ODE_DB_CONN = None

TOOL_DIR='/home/group9admin/tools'
OUTPUT_DIR='/var/www/results'

TOOLBOX_DONE=False
# //////////////////////////////////////////////////////

class Tool(object):
	
	TOOLSET_SQL = [ 
	# 0
	r'''SELECT gs_id, gsv.ode_gene_id, ode_ref_id FROM geneset_value gsv, gene gid
	WHERE gsv_in_threshold AND gs_id=ANY(%s) AND gsv.ode_gene_id=gid.ode_gene_id AND gid.ode_pref;''',

	# 1
	r'''SELECT a.ode_gene_id as "left_ode_gene_id", b.ode_gene_id as "right_ode_gene_id", a.hom_id
	FROM homology a, homology b
	WHERE a.hom_id=b.hom_id and a.ode_gene_id<>b.ode_gene_id
	 AND a.ode_gene_id IN (SELECT DISTINCT ode_gene_id FROM geneset_value WHERE gsv_in_threshold and gs_id=ANY(%s))
	 AND b.ode_gene_id IN (SELECT DISTINCT ode_gene_id FROM geneset_value WHERE gsv_in_threshold and gs_id=ANY(%s))
	GROUP BY left_ode_gene_id, right_ode_gene_id, a.hom_id
	''',

	# 2
	r'SELECT gs_id, gs_name, gs_abbreviation FROM geneset WHERE gs_id=ANY(%s);',

	# 3
	r'SELECT gs_id, sp_id, gs_gene_id_type FROM geneset WHERE gs_id=%s;',

	# 4
	r'SELECT COUNT(DISTINCT ode_gene_id) FROM geneset_value WHERE gs_id IN (%s,%s)',

	# 5
	r'''SELECT COUNT(DISTINCT ode_gene_id) FROM probe2gene p2g, platform m, probe p 
	 WHERE p2g.prb_id=p.prb_id AND p.pf_id=m.pf_id AND (m.pf_id=%s OR m.pf_set=%s)''',

	# 6
	r'''(SELECT ode_gene_id FROM probe2gene p2g, platform m, probe p 
	 WHERE p2g.prb_id=p.prb_id AND p.pf_id=m.pf_id AND (m.pf_id=%s OR m.pf_set=%s))
	    INTERSECT 
	 (SELECT ode_gene_id FROM probe2gene p2g, platform m, probe p 
	 WHERE p2g.prb_id=p.prb_id AND p.pf_id=m.pf_id AND (m.pf_id=%s OR m.pf_set=%s))'''

	]

	def __init__(self, *args, **kwargs): 
		global ODE_DB_CONN
		if not ODE_DB_CONN:
			ODE_DB_CONN = psycopg2.connect("host=bepo.ecs.baylor.edu dbname=ODE-grp1 user=group1 password=group1")
		self.cur = ODE_DB_CONN.cursor()
		self.cur.execute("set search_path to production,extsrc,odestatic;")

		self._files = {}
		self._parameters = {}
		self._results = {}
		self._progress = {}
		self._matrix = {}
		self._pairwisedeletion_matrix = {}
		
		self._start_time = time.time()
		self._last_status_time = self._start_time

		if 'default_parameters' in kwargs:
			self.default_parameters = kwargs['default_parameters']
		if 'requirements' in kwargs:
			self.requirements = kwargs['requirements']

	def __del__(self):
		self.cur.close()
		del self.cur

		del self._files
		del self._progress
		del self._matrix
		del self._pairwisedeletion_matrix
		
		del self._start_time
		del self._last_status_time
		
		for key in self._parameters.keys():
			del self._parameters[key]
		for key in self._results.keys():
			del self._results[key]
		del self._parameters
		del self._results
	
	def combine_genesets(self, gsids):
		include_homology = self._parameters['Homology']=='Included'

		gsids_str = '{%d' % (gsids[0])
		for gsid in gsids[1:]:
			gsids_str+=',%d' % (gsid)
		gsids_str += '}'
		self.cur.execute(self.TOOLSET_SQL[0], (gsids_str,))
		data1 = self.cur.fetchall()
		self.cur.execute(self.TOOLSET_SQL[1], (gsids_str,gsids_str))
		data2 = self.cur.fetchall()
		self.cur.execute(self.TOOLSET_SQL[2], (gsids_str,))
		data3 = self.cur.fetchall()

		self.update_progress("Combining GeneSets...")
		matrix = {}
		for row in data1:
			self.update_progress('', self.cur.rownumber*100/self.cur.rowcount)
			if row[1] not in matrix:
				matrix[ row[1] ] = {}
			matrix[ row[1] ][ 0 ] = row[2]
			matrix[ row[1] ][ row[0] ] = 1

		#//////////////////////////

		homologs={}
		h2 = {}
		if len(data2)>0 and include_homology:
			self.update_progress("Integrating homologous genes...")
			for row in data2:
				if row[0] not in homologs:
					homologs[ row[0] ] = {}
				if row[1] not in homologs:
					homologs[ row[1] ] = {}

				homologs[ row[0] ][ row[1] ] = 1
				homologs[ row[1] ][ row[0] ] = 1

			for (h,arr) in homologs.items():
				cnt=len(arr)+1
				if cnt not in h2:
					h2[cnt]=[]
				h2[ cnt ].append(h)

			h2counts=h2.keys()
			h2counts.sort(reverse=True)

			for cnt in h2counts:
				for candidate in h2[cnt]:
					if candidate in homologs and candidate in matrix:
						r3 = matrix[candidate]
						added=False
						for g2 in homologs[candidate].keys():
							if g2 not in matrix:
								continue
							for gsid in gsids:
								if (gsid in matrix[g2] and matrix[g2][gsid]):
									if (gsid not in r3 or not r3[gsid]):
										r3[gsid]=1
										added=True
							del matrix[g2]
							del homologs[g2]
						if added:
							matrix[-candidate]=r3
							del matrix[candidate]

		# ///////////////////////////
		matrix['==HEADER=='] = {'gsids': gsids, 'gslabels': {}, 'gsnames': {}}
		for row in data3:
			matrix['==HEADER==']['gslabels'][row[0]] = re.sub("[\t\n]",' ',row[2])
			matrix['==HEADER==']['gsnames'][row[0]] = re.sub("[\t\n]",' ',row[1])

		return matrix

	def pairwise_deletion_counting(self, matrix):
		header = matrix['==HEADER==']
		gsids = header['gsids']
		num_genesets = len(gsids)
		do_pairwise_deletion=False
		for param in self._parameters:
			if 'PairwiseDeletion' in param and self._parameters[param]=='Enabled':
				do_pairwise_deletion=True

		c = {}
		if do_pairwise_deletion:
			self.update_progress('Performing Pairwise Deletion Counting...')
		else:
			self.update_progress('Performing Pairwise Counting...')

		for i in range(0,num_genesets):
			self.update_progress('', i*25/num_genesets)
			c[i] = [False for x in range(0,num_genesets)]
			for j in range(i,num_genesets):
				ref_count = None
				ref_pop = None
				self.cur.execute(self.TOOLSET_SQL[3], (gsids[i],))
				data1 = self.cur.fetchone()
				self.cur.execute(self.TOOLSET_SQL[3], (gsids[j],))
				data2 = self.cur.fetchone()

				if not do_pairwise_deletion or data1[2]<0 or data2[2]<0 or data1[1]!=data2[1]:
					self.cur.execute(self.TOOLSET_SQL[4], (gsids[i],gsids[j]))
					data3 = self.cur.fetchone()

					ref_count=data3[0]
				elif data1[2]==data2[2]:
					self.cur.execute(self.TOOLSET_SQL[5], (data1[2],data1[2]))
					data3 = self.cur.fetchone()

					ref_count=data3[0]
				else:
					self.cur.execute(self.TOOLSET_SQL[6], (data1[2],data1[2], data2[2],data2[2]))
					ref_count=self.cur.rowcount
					data3 = self.cur.fetchall()
					ref_pop={}
					for r in data3:
						ref_pop[ r[0] ]=1
				c[i][j] = {'00': ref_count, '10': 0, '01': 0, '11': 0, 'ref': ref_pop}

		for i in range(0,num_genesets):
			gsA = gsids[i]

			self.update_progress('', 25+(i+1)*75/num_genesets)
			for (gid,row) in matrix.items():
				if gid=='==HEADER==':
					continue

				for j in range(i,num_genesets):
					gsB = gsids[j]
					if c[i][j]['ref'] and gid not in c[i][j]['ref']:
						continue

					# counting
					if gsA not in row or row[gsA]==0:
						if gsB in row and row[gsB]!=0:
							# A is missing, B is set
							c[i][j]['01']+=1
							c[i][j]['00']-=1
					else:
						if gsB not in row or row[gsB]==0:
							# A is set, B is missing
							c[i][j]['10']+=1
							c[i][j]['00']-=1
						else:
							# both are set
							c[i][j]['11']+=1
							c[i][j]['00']-=1

			for j in range(i+1,num_genesets):
				c[ j ][ i ] = dict(c[ i ][ j ])
				#c[ j ][ i ]['00'] = c[ i ][ j ]['00']
				c[ j ][ i ]['01'] = c[ i ][ j ]['10']
				c[ j ][ i ]['10'] = c[ i ][ j ]['01']
				#c[ j ][ i ]['11'] = c[ i ][ j ]['11']
		c['==HEADER==']=header
		return c

	##################

	def write_combined_file(self, filename, matrix):
		F = open(filename, "w")
		header = matrix['==HEADER==']
		gsids = header['gsids']
		newline = ["%d\t%d" % (len(matrix)-1, len(gsids))]
		newline2= ["\t"]

		for gsid in gsids:
			newline.append("GS%d" % (gsid))
			newline2.append(header['gslabels'][gsid])
		print >>F, "\t".join(newline)
		print >>F, "\t".join(newline2)

		for (geneid,row) in matrix.items():
			if geneid=='==HEADER==':
				continue

			newline = ["%d" % (geneid),row[0]]
			e=0
			for gsid in gsids:
				if gsid in row and row[gsid]:
					newline.append("1")
					e+=1
				else:
					newline.append("0")
			print >>F, "\t".join(newline)
		F.close()

	def write_pairwise_deletion_file(self, filename, pwdc):
		F = open(filename, "w")
		header = pwdc['==HEADER==']
		gsids = header['gsids']
		newline = ["%d" % (len(gsids))]
		newline2= [""]

		for gsid in gsids:
			newline.append("GS%d" % (gsid))
			newline2.append(header['gslabels'][gsid])
		print >>F, "\t".join(newline)
		print >>F, "\t".join(newline2)

		for i in range(0,len(gsids)):
			row = pwdc[i]

			newline = ["GS%d" % (gsids[i])]
			for j in range(0,len(gsids)):
				counts = row[j]
				newline.append("%d (%d) %d [%d]" % (counts['10'], counts['11'], counts['01'], counts['00']))
			print >>F, "\t".join(newline)
		F.close()


	def load_parameters(self, params):
		if isinstance(params, str):
			self._parameters = json.loads( params )
		elif isinstance(params, dict):
			self._parameters = params 
		else:
			raise "parameters must be dict() or json string"

		try:
			for (k,v) in self.default_parameters.items():
				if k not in self._parameters:
					self._parameters[k]=v
		except:
			pass

	def load_matrix_py(self, mat):
		header = mat['==HEADER==']
		gsids = header['gsids']
		self._files['matrix']=None
		self._num_genes = len(mat)
		self._num_genesets = len(gsids)
		self._gsids=[]
		self._gsnames=[]
		for gsid in gsids:
			self._gsids.append("GS%d" %(gsid))
			self._gsnames.append(header['gslabels'][gsid] )
		self._gsdescriptions = self._gsnames # TODO: actual gs_descriptions
		self._matrix = []
	 	for (geneid,row) in mat.items():
			if geneid=='==HEADER==':
				continue
			newline = [geneid,row[0]]
			self._num_edges=0
			for gsid in gsids:
				if gsid in row and row[gsid]:
					newline.append(1)
					self._num_edges+=1
				else:
					newline.append(0)
			self._matrix.append(newline)
	
	def load_pairwisedeletion_matrix_py(self, pwdc):
		header = pwdc['==HEADER==']
		gsids = header['gsids']
		self._files['pairwisedeletion_matrix']=None
		self._num_genesets = len(gsids)
		self._gsids=[]
		self._gsnames=[]
		for gsid in gsids:
			self._gsids.append("GS%d" %(gsid))
			self._gsnames.append(header['gslabels'][gsid] )
		self._gsdescriptions = self._gsnames # TODO: actual gs_descriptions
		self._pairwisedeletion_matrix = []
		for i in range(0,len(gsids)):
			self._pairwisedeletion_matrix.append([])
			row = pwdc[i]

			for j in range(0,len(gsids)):
				counts = row[j]
				self._pairwisedeletion_matrix[i].append( counts )

	def load_matrix(self, filename):
		self._files['matrix']=filename
		matrix = open(filename).read().split("\n")
		header = matrix[0].split("\t")
		self._num_genes = int(header[0])
		self._num_genesets = int(header[1])
		self._gsids = header[2:]
		self._gsnames = matrix[1].split("\t")[2:]
		self._gsdescriptions = self._gsnames # TODO: actual gs_descriptions
		self._matrix = []
		for row in matrix[2:]:
			if row=='':
				continue
			self._matrix.append( row.split("\t") )

	def load_pairwisedeletion_matrix(self, filename):
		self._files['pairwisedeletion_matrix']=filename
		pwdc = open(filename).read().split("\n")
		header = pwdc[0].split("\t")
		self._num_genesets = int(header[0])
		self._gsids = header[1:]
		self._gsnames = pwdc[1].split("\t")[1:]
		self._gsdescriptions = self._gsnames # TODO: actual gs_descriptions

		pwdc = pwdc[2:]
		self._pairwisedeletion_matrix = []
		for i in range(0,self._num_genesets):
			cnt = pwdc[i].split("\t")[1:]
			self._pairwisedeletion_matrix.append([])
			for j in range(0,self._num_genesets):
				m = re.search(r'^([0-9]*) \(([0-9]*)\) ([0-9]*) \[([0-9]*)\]$', cnt[j])
				if m:
					self._pairwisedeletion_matrix[ i ].append({
						'10': int(m.group(1)), '11': int(m.group(2)),
						'01': int(m.group(3)), '00': int(m.group(4))
					})

	def start(self, params={}):

		self._results = {'parameters': self._parameters}

		self.run()

	def get_results(self):
		return self._results

	def get_exectime(self):
		cur_time = time.time()
		elapsed = int(cur_time-self._start_time)
		return "%d:%02d" % (elapsed/60, elapsed%60)

	def _status_out(self, text):
		print >> sys.stderr, text

	def set_status_out(self, callback):
		if callback:
			self.status_out=callback
		else:
			self.status_out=self._status_out

	def update_progress(self, text, percent=0):
		if '_last_' not in self._progress:
			self._progress = {'_last_': ['',0] }
		
		if text=='':
			text=self._progress['_last_'][0]

		if float(percent)<=0.09:
			pstr='      '
		else:
			pstr="%5.1f%%" % (float(percent))

		self._progress[text]=percent

		if self._progress['_last_'][0]!=text:
			self.status_out("%s - %s %s" % (self.get_exectime(), pstr, text))
			self._last_status_time=time.time()
			self._progress['_last_'][0]=text
			self._progress['_last_'][1]=percent
		elif (percent-self._progress['_last_'][1])>=10 or (time.time()-self._last_status_time) >= 30:
			self.status_out("%s - %s %s" % (self.get_exectime(), pstr, text))
			self._last_status_time=time.time()
			self._progress['_last_'][1]=percent
		
		# print >> sys.stderr, "%s - %s %s" % (self.get_exectime(), pstr, text)

	def isready(self):
		try:
			for item in self.requirements:
				if item=='matrix':
					if len(self._matrix)<=1:
						return False
				elif item=='pairwisedeletion_matrix':
					if len(self._pairwisedeletion_matrix)==0:
						return False
				elif item not in self._parameters:
					return False
		except:
			pass
		return True

	def usage(self):
		print >> sys.stderr, "USAGE: %s" % (sys.argv[0]),
		try:
			for item in self.requirements:
				if item=='matrix':
					print >> sys.stderr, " -m=geneset_matrix.txt",
				elif item=='pairwisedeletion_matrix':
					print >> sys.stderr, " -p=pairwisedeletion_matrix.txt",
				elif item not in self._parameters:
					print >> sys.stderr, "\n Required json paramters: %s" %(item),
		except:
			pass
		print >> sys.stderr, ""
		sys.exit(1)

	# a default main() method...
	def main(self):
		#print "Parameters [ctrl-d when done]: ",
		sys.stdout.flush()
		p = sys.stdin.read().strip()
		if p=='':
			p='{}'

		self.load_parameters(p)
		for arg in sys.argv[1:]:
			d=arg.split("=", 2)
			if d[0]=='-m':
				self.load_matrix(d[1])
			if d[0]=='-p':
				self.load_pairwisedeletion_matrix(d[1])
			if d[0]=='-h':
				self.usage()

		if not self.isready():
			self.usage()

		self.start()
		print json.dumps(self._results)

	def get_results(self):
		return self._results;

	def run(self):
		raise NotImplementedError("""
		ode.Tool.run() is not implemented. Here's a short reference:
		 update status with: update_progress("what i'm doing", percent_complete)
		 place result data in: self._results[...] = {}
		
		 useful params/data for your implementation are:
		 
		     self._matrix
		     self._pairwisedeletion_matrix
		     self._parameters
		     self._num_genes
		     self._num_genesets
		     self._gsids
		     self._gsnames
		     self._gsdescriptions
		 
		     self._results
		""")

#####################################################

class Toolbox(object):

	def __init__(self):
		global ODE_DB_CONN
		if not ODE_DB_CONN:
			ODE_DB_CONN = psycopg2.connect("host=bepo.ecs.baylor.edu dbname=ODE-grp1 user=group1 password=group1")
		self.cur = ODE_DB_CONN.cursor()
		self.cur.execute("set search_path to production,extsrc,odestatic;")
		self.toolbox = {}

		self.cur.execute("SELECT tool_classname,tool_name,tool_description,tool_requirements FROM tool;")
		toollist = self.cur.fetchall()
		for t in toollist:
			desc = {'classname': t[0], 'name': t[1], 'description': t[2], 'requirements': re.split(', *',t[3]), 'default_parameters': {} }

			try:
				self.cur.execute("SELECT tp_name,tp_default FROM tool_param WHERE tool_classname=%s;", (t[0],))
				paramlist = self.cur.fetchall()
				for p in paramlist:
					desc['default_parameters'][ p[0] ] = p[1]
			except:
				pass

			try:
				desc['module'] = __import__("ode.%s" % desc['classname'], fromlist=desc['classname'])
			except:
				desc['module'] = __import__("%s" % desc['classname'], fromlist=desc['classname'])

			self.toolbox[ desc['name'] ] = desc
	
	def run_tool(self, toolbox_entry, output_prefix, gsids, params={}, status_callback=None ):
		mat=None

		tool = toolbox_entry['module'].NewTool( default_parameters=toolbox_entry['default_parameters'], requirements=toolbox_entry['requirements'] )
		tool.set_status_out(status_callback)
		
		params = dict(params)
		if 'Homology' not in params:
			params['Homology']='Included'

		params['output_prefix']=output_prefix
		tool.load_parameters(params)

		if 'matrix' in toolbox_entry['requirements']:
			mat = tool.combine_genesets(gsids)
			tool.load_matrix_py(mat)

		if 'matrix_file' in toolbox_entry['requirements']:
			tool.write_combined_file('%s.odemat' % output_prefix, mat)
		
		if 'pairwisedeletion_matrix' in toolbox_entry['requirements']:
			if not mat:
				mat = tool.combine_genesets(gsids)
			pwdc = tool.pairwise_deletion_counting(mat);
			tool.load_pairwisedeletion_matrix_py(pwdc)

		tool.start()
		import copy
		toret = copy.deepcopy(tool.get_results())
		del tool
		return toret

	def mainloop(self, OUTPUT_DIR='.'):
		global ODE_DB_CONN, TOOLBOX_DONE
		import time

		os.chdir(OUTPUT_DIR)

		requests={}

		mem_start=mem_end=memory()
		while not TOOLBOX_DONE:
			try:
				self.cur.execute("SELECT res_id, gs_ids, res_tool, res_runhash, res_data FROM result WHERE res_id=(SELECT next_queued_result()) LIMIT 1;")
				if self.cur.rowcount==1:
					row=self.cur.fetchone()
					gsids=map(int, row[1].split(','))
					print "\n   START: '%s' on %d GeneSets: %s" % (row[2],len(gsids),gsids)

					try:
						toolbox_entry = self.toolbox[ row[2] ]
					except:
						print "   ERROR: Tool '%s' not found" % row[2]

					if row[2] not in requests:
						requests[row[2]]=1
					else:
						requests[row[2]]+=1

					def _status_updater(text):
						print >> sys.stderr, "  STATUS: %s" % text
						self.cur.execute("UPDATE result SET res_status=COALESCE(res_status,'')||%s WHERE res_id=%s", ("\n%s"%text, row[0]))
						ODE_DB_CONN.commit()

					mem_start=memory()
					try:
						results = self.run_tool(toolbox_entry, row[3], gsids, json.loads(row[4]), _status_updater);
						self.cur.execute('UPDATE result SET res_data = %s,res_completed=NOW(),res_status=COALESCE(res_status,\'\')||E\'\nDONE\' WHERE res_id=%s', (json.dumps(results), row[0]))
						del results
						mem_end=memory()
						print "     END: Finished Successfully (%f kb)\n--------" % ((mem_end-mem_start)/1024.0)
					except:
						exc_type, exc_value, exc_traceback = sys.exc_info()

						print "Tool got Unexpected error:\n"
						import traceback
						traceback.print_exc()
						tb = traceback.extract_tb(exc_traceback)
						error_msg = 'Out of memory. '
						if str(exc_value)=='GraphViz choked':
							error_msg = 'GraphViz choked. '
						error_msg += 'Try something smaller. (%d)' % (tb[-1][1],)

						self.cur.execute('UPDATE result SET res_completed=NOW(),res_status=COALESCE(res_status,\'\')||E\'\nERROR - \'||%s WHERE res_id=%s', (error_msg, row[0]))
						mem_end=memory()
						print "     END: Quit with Error (%f kb)\n--------" % ((mem_end-mem_start)/1024.0)

					print "\rREQUESTS: %s (%f kb in use)" %(requests, mem_end/1024.0),
					sys.stdout.flush()

				ODE_DB_CONN.commit()

				# wait 1/2 sec between runs
				time.sleep(0.500)
			except KeyboardInterrupt:
				TOOLBOX_DONE = True
			except:
				print "Unexpected error: ", sys.exc_info()
				pass

def memory():
	try:
		t=open("/proc/%d/status" % os.getpid())
		v=t.read()
		t.close()

		i=v.index('VmSize:')
		v=v[i:].split(None,3)
		if v[2].lower()=='kb':
			return float(v[1])*1024.0
		if v[2].lower()=='mb':
			return float(v[1])*1024.0*1024.0
		if v[2].lower()=='gb':
			return float(v[1])*1024.0*1024.0*1024.0
		return float(v[1])
	except:
		return 0.0

def cleanup(sig,fonc=None):
	global TOOLBOX_DONE
	TOOLBOX_DONE=True

def makeDaemon():
	pid=os.fork()
	if pid==0:
		os.setsid()
		pid=os.fork()
		if pid!=0:
			os._exit(0)
	else:
		os._exit(0)

	import resource
	maxfd=resource.getrlimit(resource.RLIMIT_NOFILE)[1]
	if maxfd==resource.RLIM_INFINITY:
		maxfd=MAXFD
	for fd in range(0,maxfd):
		try:
			os.close(fd)
		except:
			pass

	os.open("/dev/null", os.O_RDONLY)
	os.open("tools_log.txt", os.O_RDWR|os.O_CREAT|os.O_APPEND)
	os.dup2(1,2)
	import datetime
	print "ODE Toolbox Started on %s\n--------------------" % datetime.datetime.today()

if __name__=='__main__':
	if len(sys.argv)>3:
		print "USAGE: %s [tool_output_directory]" % sys.argv[0]
		print "   or  %s --stop" % sys.argv[0]
	else:
		if os.path.exists("tools_running.pid"):
			pf=open("tools_running.pid")
			try:
				import signal
				oldpid=int(pf.readline().strip())
				os.kill(oldpid, signal.SIGTERM)
				print "Old tools.py process successfully terminated (pid %d)." % oldpid
			except:
				pass
			pf.close()
			os.unlink("tools_running.pid")

		if len(sys.argv)==2 and sys.argv[1]=='--stop':
			sys.exit()

		if os.path.exists("tools_log.txt"):
			os.unlink("tools_log.txt")
			print "existing tools_log.txt deleted"

		if len(sys.argv)==2 and sys.argv[1]=='--nobg':
			Toolbox().mainloop(OUTPUT_DIR)
		else:
			makeDaemon()

			pf=open("tools_running.pid","w")
			print >> pf, os.getpid()
			pf.close()
			import signal
			signal.signal(signal.SIGTERM, cleanup)

			if len(sys.argv)==2:
				Toolbox().mainloop(sys.argv[1])
			else:
				Toolbox().mainloop(OUTPUT_DIR)

		print "\nClean exit."
