Combining GeneSets:

  A combination matrix is produced from the union of all genes in the selected
GeneSets. Columns of the matrix represent the GeneSets, and the rows represent
genes. When a gene is contained in a set, a 1 is placed in the corresponding
position, otherwise a 0 is used. 
  Homology information is retrieved for all the genes in consideration. Genes
with the largest number of homology relationships to other genes are selected
as homology cluster centers, and all the related gene rows of the matrix are
merged into the cluster. This process continues until no remaining genes have
homology.

Pairwise Deletion Counting:

  If either GeneSet is not from a microarray, or the GeneSets are from microarrays in different species,
     then the reference population is the union of genes in either GeneSet (including any below threshold)
  
  If both GeneSets are from the same microarray,
     then the reference population is the genes on that microarray (ie "Microarray U74 v2")

  If both GeneSets are from microarrays in the same species, 
     then the reference population is the intersections of the genes on both microarrays
     (ie "genes on both U74 v2 _and_ MOE430") Genes not in this set are discarded from calculations.

