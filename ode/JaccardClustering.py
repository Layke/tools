#!/usr/bin/python
# USAGE: JaccardClustering.py input.odemat < parameters_json.txt > output_json.txt 2>status.txt

import os
import subprocess

from tools import Tool

CLUSTER_RSCRIPT = '''
.libPaths("/home/jjay/R/x86_64-unknown-linux-gnu-library/2.11"); library(made4);

myheatmap <- function (x, Rowv = NULL, Colv = if (symm) "Rowv" else NULL, 
		distfun = dist, hclustfun = hclust, reorderfun = function(d, 
				w) reorder(d, w), add.expr, symm = FALSE, revC = identical(Colv, 
				"Rowv"), scale = c("row", "column", "none"), na.rm = TRUE, 
		margins = c(5, 5), ColSideColors, RowSideColors, cexRow = 0.2 + 
				1/log10(nr), cexCol = 0.2 + 1/log10(nc), labRow = NULL, 
		labCol = NULL, main = NULL, xlab = NULL, ylab = NULL, keep.dendro = FALSE, 
		verbose = getOption("verbose"), ...) 
{
		# col=heat.colors(255);
		col=c(rainbow(255, start = 1/6, end = 4/6, alpha = 1));
		scale <- if (symm && missing(scale)) 
				"none"
		else match.arg(scale)
		if (length(di <- dim(x)) != 2 || !is.numeric(x)) 
				stop("'x' must be a numeric matrix")
		nr <- di[1]
		nc <- di[2]
		if (nr <= 1 || nc <= 1) 
				stop("'x' must have at least 2 rows and 2 columns")
		if (!is.numeric(margins) || length(margins) != 2) 
				stop("'margins' must be a numeric vector of length 2")
		doRdend <- !identical(Rowv, NA)
		doCdend <- !identical(Colv, NA)
		if (is.null(Rowv)) 
				Rowv <- rowMeans(x, na.rm = na.rm)
		if (is.null(Colv)) 
				Colv <- colMeans(x, na.rm = na.rm)
		if (doRdend) {
				if (inherits(Rowv, "dendrogram")) 
						ddr <- Rowv
				else {
						hcr <- hclustfun(distfun(x))
						ddr <- as.dendrogram(hcr)
						if (!is.logical(Rowv) || Rowv) 
								ddr <- reorderfun(ddr, Rowv)
				}
				if (nr != length(rowInd <- order.dendrogram(ddr))) 
						stop("row dendrogram ordering gave index of wrong length")
		}
		else rowInd <- 1L:nr
		if (doCdend) {
				if (inherits(Colv, "dendrogram")) 
						ddc <- Colv
				else if (identical(Colv, "Rowv")) {
						if (nr != nc) 
								stop("Colv = \\"Rowv\\" but nrow(x) != ncol(x)")
						ddc <- ddr
				}
				else {
						hcc <- hclustfun(distfun(if (symm) 
								x
						else t(x)))
						ddc <- as.dendrogram(hcc)
						if (!is.logical(Colv) || Colv) 
								ddc <- reorderfun(ddc, Colv)
				}
				if (nc != length(colInd <- order.dendrogram(ddc))) 
						stop("column dendrogram ordering gave index of wrong length")
		}
		else colInd <- 1L:nc
		x <- x[rowInd, colInd]
		labRow <- if (is.null(labRow)) 
				if (is.null(rownames(x))) 
						(1L:nr)[rowInd]
				else rownames(x)
		else labRow[rowInd]
		labCol <- if (is.null(labCol)) 
				if (is.null(colnames(x))) 
						(1L:nc)[colInd]
				else colnames(x)
		else labCol[colInd]
		if (scale == "row") {
				x <- sweep(x, 1, rowMeans(x, na.rm = na.rm), check.margin = FALSE)
				sx <- apply(x, 1, sd, na.rm = na.rm)
				x <- sweep(x, 1, sx, "/", check.margin = FALSE)
		}
		else if (scale == "column") {
				x <- sweep(x, 2, colMeans(x, na.rm = na.rm), check.margin = FALSE)
				sx <- apply(x, 2, sd, na.rm = na.rm)
				x <- sweep(x, 2, sx, "/", check.margin = FALSE)
		}
		lmat <- rbind(c(4, 3), 2:1)
		lwid <- c(if (doRdend) 1 else 0.05, 4)
		lhei <- c((if (doCdend) 1 else 0.05) + if (!is.null(main)) 0.2 else 0, 
				4)
		if (!missing(ColSideColors)) {
				if (!is.character(ColSideColors) || length(ColSideColors) != 
						nc) 
						stop("'ColSideColors' must be a character vector of length ncol(x)")
				lmat <- rbind(lmat[1, ] + 1, c(NA, 1), lmat[2, ] + 1)
				lhei <- c(lhei[1], 0.2, lhei[2])
		}
		if (!missing(RowSideColors)) {
				if (!is.character(RowSideColors) || length(RowSideColors) != 
						nr) 
						stop("'RowSideColors' must be a character vector of length nrow(x)")
				lmat <- cbind(lmat[, 1] + 1, c(rep(NA, nrow(lmat) - 1), 
						1), lmat[, 2] + 1)
				lwid <- c(lwid[1], 0.2, lwid[2])
		}
		lmat[is.na(lmat)] <- 0
		if (verbose) {
				cat("layout: widths = ", lwid, ", heights = ", lhei, 
						"; lmat=\n")
				print(lmat)
		}
		op <- par(no.readonly = TRUE)
		on.exit(par(op))
		layout(lmat, widths = lwid, heights = lhei, respect = TRUE)
		if (!missing(RowSideColors)) {
				par(mar = c(margins[1], 0, 0, 0.5))
				image(rbind(1L:nr), col = RowSideColors[rowInd], axes = FALSE)
		}
		if (!missing(ColSideColors)) {
				par(mar = c(0.5, 0, 0, margins[2]))
				image(cbind(1L:nc), col = ColSideColors[colInd], axes = FALSE)
		}
		par(mar = c(margins[1], 0, 0, margins[2]))
		if (!symm || scale != "none") 
				x <- t(x)
		if (revC) {
				iy <- nr:1
				ddr <- rev(ddr)
				x <- x[, iy]
		}
		else iy <- 1L:nr
		image(1L:nc, 1L:nr, x, xlim = 0.5 + c(0, nc), ylim = 0.5 + 
				c(0, nr), axes = FALSE, xlab = "", ylab = "", col=col, ...)
		for(i in 2:length(ColSideColors)) {
			 print( sprintf("%%d (%%s) != %%d (%%s)", i, ColSideColors[i], i-1, ColSideColors[i-1]) );
			 if( ColSideColors[colInd[i]] != ColSideColors[colInd[i-1]] ) {
					abline(v=(i-0.5), col="grey");
					print( "true" );
			 }
		}
		oldps = par("ps");
		ps <- as.integer(72*(par("pin")[2]/nr));
		if( ps < 6 ) ps=6;
		if( ps > 14 ) ps=14;
		par(ps=ps);
		axis(1, 1L:nc, labels = labCol, las = 2, line = -0.5, tick = 0, 
				cex.axis = cexCol)
		if (!is.null(xlab)) 
				mtext(xlab, side = 1, line = margins[1] - 1.25)
		axis(4, iy, labels = labRow, las = 2, line = -0.5, tick = 0, 
				cex.axis = cexRow)
		if (!is.null(ylab)) 
				mtext(ylab, side = 4, line = margins[2] - 1.25)
		if (!missing(add.expr)) 
				eval(substitute(add.expr))
		par(ps=oldps);
		par(mar = c(margins[1], 0, 0, 0))
		if (doRdend) 
				plot(ddr, horiz = TRUE, axes = FALSE, yaxs = "i", leaflab = "none")
		else frame()
		par(mar = c(0, 0, 1, margins[2]))
		if (doCdend) 
				plot(ddc, axes = TRUE, xaxs = "i", leaflab = "none")
		else if (!is.null(main)) 
				frame()
		if (!is.null(main)) {
				par(xpd = NA)
				title(main, cex.main = 1.5 * op[["cex.main"]])
		}
		par(mar = c(5, 0.5, 5, 5),cex=0.5)
		z = seq(0.0, 1.0, length=length(col));
		b = seq(0.0, 1.0, length=length(col)+1);
		image(z=matrix(z,ncol=1),col=col,breaks=b,xaxt="n",yaxt="n");
		lv=pretty(z);
		xv=as.numeric(lv);
		axis(1,at=xv,labels=lv);
		mtext("Jaccard Dissimilarity",side=1,line=2,cex=0.6);
		
		invisible(list(rowInd = rowInd, colInd = colInd, Rowv = if (keep.dendro && 
				doRdend) ddr, Colv = if (keep.dendro && doCdend) ddc))
}

clusterfunc <- function(table, method) {
	pdf("%s", "pdf", height=10, width=10);
	
	if( method == "K-Means" ) {
		 mycl <- hclust(as.dist(table), method='complete');
		 cutted <- cutree(mycl, h=max(mycl$height)/1.5);
		 mycol <- sample(rainbow(256)); mycol <- mycol[as.vector(cutted)];

		 mycl <- kmeans(as.dist(table), max(cutted));
		 mycol <- sample(rainbow(256)); mycol <- mycol[as.vector(mycl$cluster)];
		 heatmap(as.matrix(table), Rowv=as.vector(mycl$cluster), Colv=as.vector(mycl$cluster), scale="row", RowSideColors=mycol, symm=TRUE);
		 as.vector(mycl$cluster)
	} else if( method == "QT Clust" ) {
		 library(flexclust);
		 mycl <- qtclust(as.dist(table), radius=3);
		 mycol <- sample(rainbow(256)); mycol <- mycol[as.vector(mycl$cluster)];
	} else {
		 mycl <- hclust(as.dist(table), method=method);
		 cutted <- cutree(mycl, k=%d);
		 mycol <- sample(rainbow(256)); mycol <- mycol[as.vector(cutted)];
		 m=max(nchar(rownames(table)));
		 h=myheatmap(as.matrix(table), Rowv=as.dendrogram(mycl), Colv=as.dendrogram(mycl), scale="row", ColSideColors=mycol, symm=TRUE, margins=c(20,20));
	}
	
	dev.off()
}

clusterfunc( read.table( "%s" ), '%s' )

'''  # % ( pdf_filename, k, input_filename, method )


#########################################################

class JaccardClustering(Tool):
	def __init__(self, *args, **kwargs): 
		super(JaccardClustering, self).__init__(*args, **kwargs)
		self.urlroot=''

	def run(self):
		method = self._parameters['JaccardClustering_Method'].lower()
		output_prefix = self._parameters["output_prefix"]   # {.el,.dot,.png}
		
		num_edges=0
		
		self.update_progress("Computing Jaccard Dissimilarities...")
		i=0;
		JSC=[]
		for i in range(0,self._num_genesets):
			self.update_progress('', (i+1)*100/self._num_genesets )
			JSC.append([])
			for j in range(0,self._num_genesets):
				m = self._pairwisedeletion_matrix[i][j]
				
				union = m['01'] + m['10'] + m['11']
				if union==0:
					similarity=0
				else:
					similarity=float(m['11'])/float(union)
				
				JSC[ i ].append( 1.0-similarity )
		
		k=self._num_genesets/3;
		
		self.update_progress("Clustering GeneSets...")
		
		jout = open("%s.jac" % (output_prefix), "w")
		
		clabels = {}
		i=0
		for gsid in self._gsids:
			clabels[i]='"%s: %s"' % (gsid,self._gsnames[i]) 
			print >> jout, "\t%s" % clabels[i],
			i+=1
		print >> jout
		
		i=0
		for row in JSC:
			print >> jout, clabels[i],
			for jac in row:
				print >> jout, "\t%f" % (jac),
			print >> jout
			i+=1
		jout.close()
		
		proc = subprocess.Popen("R -q --vanilla --slave", shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		print >> proc.stdin, CLUSTER_RSCRIPT % (output_prefix+".pdf", k, output_prefix+".jac", method)
		proc.stdin.close()
		proc.wait()
		
		# rasterize pdf to png
		os.system("gs -q -dSAFTER -dBATCH -dNOPAUSE -sDEVICE=png16m -dGraphicsAlphaBits=4 -dTextAlphaBits=4 -r200 -dBackgroundColor='16#ffffff' -sOutputFile=%s.png %s.pdf" % (output_prefix, output_prefix) );
		
		self._results['result_image']="%s.png" % (output_prefix)
		self._results['result_pdf']="%s.pdf" % (output_prefix)

def NewTool(*args, **kwargs):
	return JaccardClustering(*args, **kwargs)

if __name__ == '__main__':
	JaccardClustering().main()
