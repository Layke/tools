#!/usr/bin/python
# USAGE: JaccardClustering.py input.odemat < parameters_json.txt > output_json.txt 2>status.txt

import re
import math 

from tools import Tool

class JaccardSimilarity(Tool):
	def __init__(self, *args, **kwargs): 
		super(JaccardSimilarity, self).__init__(*args, **kwargs)
		self.urlroot=''

#  width, height
#
# html:
#   csv_url
#   svg_url
#   convert_url
#   usage_message
#   geneset_table[] = (gsID, url, text, entries)
#      ...entries[] = (url?,text}
#
# venn:
#   geneset_labels[] = (url,txA,tyA,txB,tyB,text)
#   venn_diagrams[] = (url,tx,ty, c1x,c1y,r1,title1,desc1, c2x,c2y,r2,title2,desc2, text)
#         ...text[] = (tx,ty,text)
	def factorial(self,n):
		if n<=1:
			return 1L
		n=long(n)
		f=n
		while n>1L:
			n-=1L
			f*=n
		return f

	def jac_pvalue(self,A,B,C):
		if A==0 and B==0 and C!=0:
			return 1.0
		if C==0:
			return 0.0
		N=A+B+C
		J=float(C)/float(N)
		nfac=self.factorial(N)
		pabc=math.pow(3.0,-N)
		if pabc==0:
			return 0.0
		sum=0.0
		for c in range(0,int(C+1)):
			fc=self.factorial(c)
			for a in range(max(0,int(A-C)),int(A+C+1)):
				if c+a>N:
					break

				fa=self.factorial(a)
				for b in range(max(0,int(B-C)),int(B+C+1)):
					if a+b+c < N:
						continue
					if a+b+c > N:
						break	
					if a+b+c == N:
						fb=self.factorial(b)
						fabc=fa*fb*fc
						#print "%s / %s * %f = " % (nfac,fabc,pabc),
						try:
							p=float(nfac/fabc) * pabc
						except OverflowError:
							return 1.0
						#print "%5d %5d %5d -- J=%f (p=%s)" % (a,b,c, float(c)/float(a+b+c), p)
						sum+=p
		return sum


#####################################
	def venn_circles(self, i,ii,j, size=100):
		pi = math.acos(-1.0)
		r1 = math.sqrt(i/pi)
		r2 = math.sqrt(ii/pi)
		if r1==0:
			r1=1.0
		if r2==0:
			r2=1.0
		scale=size/2.0/(r1+r2)
	
		if i==j or ii==j:  # complete overlap
			c1x=c1y=c2x=c2y=size/2.0
		elif j==0:  # no overlap
			c1x=c1y=r1*scale
			c2x=c2y=size-(r2*scale)
		else:
			# originally written by zuopan, rewritten a number of times
			step = .001
			beta = pi
			if r1<r2:
				r2_=r1
				r1_=r2
			else:
				r1_=r1
				r2_=r2
			r2o1=r2_/r1_
			r1_2=r1_*r1_
			r2_2=r2_*r2_
	
			while beta > 0:
				beta -= step
				alpha = math.asin(math.sin(beta)/r1_ * r2_)
				Sj = r1_2*alpha + r2_2*(pi-beta) - 0.5*(r1_2*math.sin(2*alpha) + r2_2*math.sin(2*beta));
				if Sj > j: 
					break
	
			oq= r1_*math.cos(alpha) - r2_*math.cos(beta)
			oq=(oq*scale)/2.0
			
			c1x=(size/2.0) - oq
			c2x=(size/2.) + oq
			c1y=c2y=size/2.0
	
		r1=r1*scale
		r2=r2*scale
	
		return {'c1x':c1x,'c1y':c1y,'r1':r1, 'c2x':c2x,'c2y':c2y,'r2':r2}

#####################################

	def run(self):
		output_prefix = self._parameters["output_prefix"]
		include_homology = self._parameters['JaccardSimilarity_Homology']=='Included'
		emphasize_set={}
		do_emphasis_check=0
		try:
			emphasis_genes = self._parameters['emphasis_genes']
			for i in range(0,len(emphasis_genes)):
				emphasis_genes[i]=int(emphasis_genes[i]);
			for r in self._matrix:
				if int(r[0]) in emphasis_genes or -int(r[0]) in emphasis_genes:
					i=0
					for e in r[2:]:
						if int(e):
							emphasize_set[i]=1
						i+=1 
			do_emphasis_check=len(emphasis_genes)>0
		except:
			pass
		
		num_edges=0
		
		leftpad  = 300
		rightpad = 150
		toppad   = 225
		vsize    = 75
					
		width=height=self._num_genesets*120
		width+=leftpad+rightpad
		height+=toppad
		
		geneset_table = []
		geneset_labels = []
		venn_diagrams = []
		for i in range(0,self._num_genesets):
			trow = {}
			lrow = {}
			trow['url'] = "index.php?action=manage&cmd=viewGeneSet&gsID=%s" % (self._gsids[i][2:])
			trow['text'] = "%s: %s" % (self._gsids[i], self._gsnames[i])
			lrow['url'] = trow['url'] 
			lrow['text'] = trow['text']
			trow['gsID'] = self._gsids[i]
			trow['entries'] = []
			lrow['txA']=i*120+60+leftpad
			lrow['tyA']=toppad
			lrow['txB']=leftpad
			lrow['tyB']=i*120+60+toppad
		
			ty=lrow['tyB']-47.5
			for j in range(0,self._num_genesets):
				venn = {'text': []}
				dcounts=self._pairwisedeletion_matrix[i][j]
				if dcounts['11']!=0:
					jac = float( dcounts['11']) / (float(dcounts['10'])+float(dcounts['01'])+float(dcounts['11']) )
					pvalue = self.jac_pvalue(dcounts['10'], dcounts['01'], dcounts['11'])
				else:
					jac=0.0
					pvalue = 1.0
				scoreh = re.sub(r'0+$', '0', "J = %4.4f" % ( jac ) )
				pval   = re.sub(r'0+$', '0', "p = %4.4f" % ( min(1.0-pvalue,pvalue) ) )
				if pvalue<=0.05:
					scoreh+='*'
				if jac!=0.0 and pvalue>=0.95:
					scoreh+='**'
		
				if i!=j:
					venn['url']='?action=analyze&amp;cmd=intersection&amp;gsids=%s,%s&amp;refpop=%s,%s,%s' % (self._gsids[i][2:], self._gsids[j][2:], dcounts['10'], dcounts['01'], dcounts['11'] )
					if include_homology:
						venn['url']+="&amp;homology=Included"
				else:
					venn['url']='?action=manage&amp;cmd=viewGeneSet&amp;gsID=%s' % (self._gsids[i][2:])
		
				venn['tx']=j*120+12.5+leftpad
				venn['ty']=ty
				venn['opacity']=0.30
				if not do_emphasis_check or i in emphasize_set:
					venn['opacity']+=0.25
				if not do_emphasis_check or j in emphasize_set:
					venn['opacity']+=0.25
				if venn['opacity']==0.80:
					venn['opacity']=1.0
				venn['title1'] = "%s (red) vs %s (blue)" % (self._gsnames[i], self._gsnames[j]);
				venn['desc1'] = "%s genes in common, %s genes only in red, %s genes only in blue" % (dcounts['11'], dcounts['10'], dcounts['01'] )
				
				x = int(dcounts['10'])
				y = int(dcounts['01'])
				z = int(dcounts['11'])
				cc = self.venn_circles(x+z,y+z,z, vsize)
				venn['c1x']=cc['c1x']
				venn['c1y']=cc['c1y']
				venn['c2x']=cc['c2x']
				venn['c2y']=cc['c2y']
				venn['r1']=cc['r1']
				venn['r2']=cc['r2']
		
				if z==0: # no intersection
					venn['text'].append({ 'tx': (cc['c1x'])*(4.0/3.0), 'ty': (cc['c1y']+cc['r1']+20)*(4.0/3.0), 'text': "(%d)" % (x) })
					venn['text'].append({ 'tx': (cc['c2x'])*(4.0/3.0), 'ty': (cc['c2y']-cc['r2']-10)*(4.0/3.0), 'text': "(%d)" % (y) })
				else:
					m="(%d)" % (z)
					if x!=0:
						m="(%d %s" % (x, m)
					if y!=0:
						m="%s %d)" % (m, y)
		
					r2r1=cc['r1']
					if cc['r2']>cc['r1']:
						r2r1=cc['r2']
		
					venn['text'].append({ 'tx': (vsize/2)*(4.0/3.0), 'ty': (vsize/2 - (r2r1) -5)*(4.0/3.0), 'text': m })
					venn['text'].append({ 'tx': 50, 'ty': 100, 'text': scoreh});
					venn['text'].append({ 'tx': 50, 'ty': 120, 'text': pval});
		
				te = {}
				if i==j:
					 te['background'] = 'background-color: #C7D0C7;'
				elif j%2==0:
					 te['background'] = 'background-color: #FFFFFF;'
				else:
					 te['background'] = 'background-color: #E7F0E7;'
		
				if jac==0:
					 te['text']=scoreh
				else:
					 heat='#666666'
					 if jac >= 0.75:
							heat='#FF0000'
					 elif jac >= 0.50:
							heat='#FF3333'
					 elif jac >= 0.25:
							heat='#33337F'
		
					 te['url']  = venn['url']
					 te['text'] = '<b style="color: %s;">%s<br />%s</b>' % (heat, scoreh, pval)
				
				trow['entries'].append(te)
				venn_diagrams.append(venn)
		
			geneset_table.append(trow)
			geneset_labels.append(lrow)
		
		self._results['geneset_table']=geneset_table
		self._results['geneset_labels']=geneset_labels
		self._results['venn_diagrams']=venn_diagrams
		self._results['width']=width
		self._results['height']=height

def NewTool(*args, **kwargs):
	return JaccardSimilarity(*args, **kwargs)

if __name__ == '__main__':
	JaccardSimilarity().main()
