#!/usr/bin/python
# USAGE: PhenomeMap.py -m=input.odemat < parameters_json.txt > output_json.txt 2>status.txt

import os
import re
import sys
import math
import subprocess

from tools import Tool, TOOL_DIR

class PhenomeMap(Tool):
	def __init__(self, *args, **kwargs):
		super(PhenomeMap, self).__init__(*args, **kwargs)
		self.urlroot=''
		self._comb_cache={}

	def combtl(self, n,r):
		if r==0:
			return 1
		key="%d,%d" % (n,r)
		if key in self._comb_cache:
			return self._comb_cache[key]

		r=long(min(r,n-r))
		i=long(n)-r+1L
		j=1L
		cnr=1L
		while i<=n or j<=r:
			if i<=n:
				cnr*=i
				i+=1L
			if j<=r:
				cnr/=j
				j+=1L

		self._comb_cache[key]=cnr
		return cnr

	def run(self):
		global TOOL_DIR
		output_prefix = self._parameters["output_prefix"]	 # {.el,.dot,.png}
		include_homology = self._parameters['PhenomeMap_Homology']=='Included'

		maxLevel = int(self._parameters["PhenomeMap_MaxLevel"])
		minGenes = int(self._parameters["PhenomeMap_MinGenes"])
		maxInNode = int(self._parameters["PhenomeMap_MaxInNode"])
		permuN = int(self._parameters['PhenomeMap_Permutations'])
		permuLimit = int(self._parameters['PhenomeMap_PermutationTimeLimit'])
		bootstrap_disabled = self._parameters['PhenomeMap_DisableBootstrap']=='True'
		emphasis_genes=[]
		do_emphasis_check=0
		if 'emphasis_genes' in self._parameters:
			emphasis_genes = self._parameters['emphasis_genes']
			for i in range(0,len(emphasis_genes)):
				emphasis_genes[i]=int(emphasis_genes[i]);
			do_emphasis_check=len(emphasis_genes)

		num_edges=0

		genes = {}
		FOUT = open("%s.el" % (output_prefix), "w")
		print >> FOUT, "%d\t%d\t%d" % (self._num_genes, self._num_genesets, num_edges)

		for r in self._matrix:
			genes[ int(r[0]) ] = r[1]
			i=0
			for e in r[2:]:
				if int(e):
					print >> FOUT, "%s\t%s" % (r[0], self._gsids[i])
				i+=1

		FOUT.close()

		self.update_progress("Running Biclique algorithm...")

		args = ("%s/TOOLBOX/biclique_tool/biclique" % (TOOL_DIR),"%s.el" % (output_prefix), "-p")
		print args;
		proc = subprocess.Popen(args, stdout=subprocess.PIPE)
		#bicfile = open("%s.bic" % output_prefix, "w")
################################################################################
################################################################################
################################################################################
		bicliques=[]
		bysize={}
		bymembers={}
		genesets={}
		genesyms={}
		maxsize=0
		i=0
		for line in proc.stdout:
			line=line.strip().split("\t")
			i+=1
			if i%3==1:
				genesets=frozenset(line)
				# sort geneset ids so they are easy to iterate
			elif i%3==2:
				emphasize=0
				genesyms={}
				for g in line:
					ode_gene_id=int(g)
					if do_emphasis_check==0 or (ode_gene_id in emphasis_genes or -ode_gene_id in emphasis_genes):
						emphasize+=1
					genesyms[ genes[ode_gene_id] ]=1
				if len(genesyms)<minGenes:
					continue
				genesyms = frozenset(genesyms)
				#print >>bicfile, "\t".join(genesyms)
				#print >>bicfile, "\t".join(genesets)

				size=len(genesets)
				if size>maxsize:
					maxsize=size

				# get names for genesets and ,-list of geneset ids
				gsidstr=''
				toplabel=''
				for gs in genesets:
					gsid=gs[2:]
					if gsidstr!='':
						gsidstr+=','
					gsidstr+=gsid

					for x in range(0,len(self._gsids)):
						if self._gsids[x]==gs:
							toplabel+="%s: %s\\n" % (gs, self._gsnames[x])

				# add biclique to node list
				genelabel=''
				if len(genesyms)<=maxInNode:
					x=0
					for sym in genesyms:
						if x!=0:
							genelabel+=', '
						if x%5==4:
							genelabel+="\\n"
						genelabel+=sym
						x+=1
				else:
					genelabel="%d genes" % (len(genesyms))

				self.cur.execute('SELECT COUNT(DISTINCT pub_id) FROM geneset where gs_id=ANY(%s);', ('{'+gsidstr+'}',))
				data1 = self.cur.fetchone()
				publabel='%d publications\\n' % (data1[0],)

				if len(genesets)==1:
					URL="%s/index.php?action=manage&cmd=viewgeneset&gs_id=%s" % (self.urlroot,gsidstr)
				else:
					URL="%s/index.php?action=analyze&cmd=intersection&gsids=%s" %(self.urlroot,gsidstr)
					if include_homology:
						URL+="&homology=Included"

				meta = {
					'emphasize': emphasize>0,
					'genesets': genesets,
					'genesyms': genesyms,
					'size': size,
					'label': "%s\\n %s(%s)" % (toplabel,publabel,genelabel),
					'URL': URL,
					'touched': 0,
					'displayed': True,
					'parents': set(),
					'children': set(),
					'nephews': set()
				}

				if size not in bysize:
					bysize[ size ]=[]
				bysize[ size ].append(len(bicliques))
				bymembers[ genesets ] = len(bicliques)
				bicliques.append( meta )

		############################################
		#bicfile.close()
		num_bicliques=len(bicliques)
	
		new_num_bicliques=0
		cut_depth=0
		def find_cut_depth():
			if maxLevel==0:
				return num_bicliques, 0
			total=0
			# find the cut-depth for the tree
			for sz in range(maxsize,0,-1):
				if sz not in bysize:
					continue

				levelcount=0
				for me in bysize[sz]:
					if bicliques[me]['displayed']:
						levelcount+=1
				total+=levelcount
				if levelcount>maxLevel:
					cut_depth=sz
					prevnotes = ''
					if 'notes' in self._results:
						prevnotes = self._results['notes']+'<br/><br/>'
					self._results['notes']="""%sThere are %d %d-way intersections, so we
						cut the tree before this level in order to keep the results
						manageable. (MaxLevel=%d)""" % (prevnotes, levelcount, sz, maxLevel)
					return total, cut_depth
			return num_bicliques, 0
		new_num_bicliques, cut_depth = find_cut_depth()

		bstrap = {'nodes': {}, 'edges': {}}
		if new_num_bicliques>100 and not bootstrap_disabled:
			self._results['notes']="""Your original Phenome Map contained more
					than 100 distinct intersections, so we applied a
					Bootstrapping reduction filter to reduce the complexity.
					Bootstrapping was done for 1000 iterations with a 75%
					sampling rate. Displayed nodes and blue edges passed a 50%
					cutoff threshold, and descendent nodes were added to fill out
					the graph structure. (set DisableBootstrap=True to skip bootstrapping)
					"""
			self.update_progress("Large graph, applying bootstrapping...")
			bicfile = open("%s.bic" % output_prefix, "w")
			for i in range(0,num_bicliques):
				print >>bicfile, "\t".join(bicliques[i]['genesyms'])
				print >>bicfile, "\t".join(bicliques[i]['genesets'])
				bicliques[i]['displayed']=False
			bicfile.close()
			args = ("%s/TOOLBOX/bstrap/bstrap" % (TOOL_DIR), '%s.bic' % output_prefix, "x", "-i", "1000 0.75", "-t", "12")
			proc = subprocess.Popen(args, stderr=subprocess.PIPE)
			bstrap_set='nodes'
			for line in proc.stderr:
				e = line.strip()
				if e=='' and bstrap_set=='nodes':
					bstrap_set='edges'
					continue
				if e=='' and bstrap_set=='edges':
					bstrap_set='nodes'
					continue

				if bstrap_set=='nodes':
					bicliques[int(e)]['displayed']=True
				if bstrap_set=='edges':
					e=e.split("\t")
					bstrap['edges'][ (int(e[0]),int(e[1])) ] = float(e[2])
			new_num_bicliques = find_cut_depth()

		############################################

		self.update_progress("Determining Subset Relationships...")
		self.num_touched=0

		# recursive, but uses 'touched' value to memoize calls
		def subset_recur(me):
			meta=bicliques[me]
			kids=set()
			nephews=set()
			for sz in range(meta['size']-1,0,-1):
				if sz not in bysize:
					continue
				for bid in bysize[sz]:
					if bicliques[bid]['genesets'] < meta['genesets']:
						if kids.isdisjoint(bicliques[bid]['parents']) and nephews.isdisjoint(bicliques[bid]['parents']):
							bicliques[bid]['parents'].add(me)
							kids.add(bid)
							if bicliques[bid]['displayed'] and not bicliques[bid]['touched']:
								nephews.update( subset_recur(bid) )
							else:
								nephews.update( bicliques[bid]['children'] )
								nephews.update( bicliques[bid]['nephews'] )
			meta['children'].update(kids)
			meta['nephews'].update(nephews)
			meta['touched']=1

			self.num_touched+=1
			self.update_progress('',self.num_touched*100.0/num_bicliques)
			kids.update(nephews)
			return kids

		# start with small nodes to keep stack depth small
		for sz in range(1,maxsize+1):
			if sz not in bysize:
				continue
			for me in bysize[sz]:
				if bicliques[me]['displayed'] and not bicliques[me]['touched']:
					subset_recur(me)

################################################################################
################################################################################
################################################################################
		self.update_progress("Building Trees...")

		FOUT = open("%s.dot" % (output_prefix), "w")
		print >> FOUT, "digraph G{"
		# splines go around nodes cleanly, epsilon and maxiter ensure it runs fast
		print >> FOUT, "	rankdir=TB;"
		print >> FOUT, "	splines=true;"
		print >> FOUT, "	epsilon=.001; maxiter=1500;"
		#print >> FOUT, "	size=\"12,12\";"
		# output defaults for all nodes and edges
		if do_emphasis_check:
			print >> FOUT, "	node [shape=box, style=filled, color=black, fillcolor=\"#ccccff\", fontname=\"Helvetica, Arial, sans-serif\", fontsize=\"10px\"];"
		else:
			print >> FOUT, "	node [shape=box, style=filled, color=black, fillcolor=\"#ccffcc\", fontname=\"Helvetica, Arial, sans-serif\", fontsize=\"10px\"];"
		print >> FOUT, "	edge [color=black];"
		print >> FOUT, "	ranksep=2;"

		#output all nodes
		for size in range(cut_depth+1,maxsize+1):
			if size not in bysize:
				continue
			print >> FOUT, " {rank=same;"
			for i in bysize[size]:
				if not bicliques[i]['displayed']:
					continue
				has_parents=''
				if len(bicliques[i]['parents'])==0:
					has_parents=", color=red"
				if has_parents=='' or len(bicliques[i]['children'])!=0:
					if bicliques[i]['emphasize'] and bicliques[i]['displayed']:
						print >> FOUT, 'Ph_node_%s [label="%s", target="_parent", URL="%s"%s];' % (i, bicliques[i]['label'], bicliques[i]['URL'], has_parents)
					else:
						print >> FOUT, 'Ph_node_%s [label="%s", fillcolor="#ccffcc", tooltip="%s", target="_parent", URL="%s"%s];' % (i, "\\n".join(bicliques[i]['genesets']), bicliques[i]['label'], bicliques[i]['URL'], has_parents)
			print >> FOUT, "}"

		# output all edges
		for i in range(0,num_bicliques):
			if len(bicliques[i]['children'])!=0 and bicliques[i]['size']>cut_depth:
				for ch in bicliques[i]['children']:
					if bicliques[ch]['size']>cut_depth and bicliques[ch]['displayed'] and bicliques[i]['displayed']:
						if (i,ch) in bstrap['edges'] and bstrap['edges'][(i,ch)] > 0.0:
							if bstrap['edges'][(i,ch)] > 0.5:
								print >> FOUT, "Ph_node_%s->Ph_node_%s [label=\"%s\", color=blue];" % (i, ch, bstrap['edges'][ (i,ch) ])
							else:
								print >> FOUT, "Ph_node_%s->Ph_node_%s [label=\"%s\"];" % (i, ch, bstrap['edges'][ (i,ch) ])
						else:
							print >> FOUT, "Ph_node_%s->Ph_node_%s;" % (i, ch)

		print >> FOUT, "}"
		FOUT.close()

		#		////////////////////////////////////////////////
		self.update_progress("Drawing Trees...")
		gvprog="dot"
		gvprog="%s/TOOLBOX/dot_wrapper.sh" % (TOOL_DIR)

		os.system("%s %s.dot -Tsvg -o %s.svg" % (gvprog, output_prefix,output_prefix))
		image_FPN=output_prefix

		proc = subprocess.Popen("%s %s.dot -Tcmap" % (gvprog, output_prefix), shell=True, stdout=subprocess.PIPE)

		self._results['result_image']="%s.svg" % (image_FPN)
		self._results['result_map']=proc.stdout.read()

		# post-process SVG file
		svgfn = self._results['result_image']
		try:
			svgin = open("%s" % svgfn)
		except IOError:
			raise Exception('GraphViz choked')
		svgout = open("%s.tmp" % svgfn,"w")
		ln=0
		for line in svgin:
			line=line.strip()
			#if ln==8:
			#	print >> svgout, '''<svg width="100%" height="100%"
			#			zoomAndPan="disable" '''
			if line=="</svg>":
				print >> svgout, '<rect id="zoomhi" fill-opacity="0" width="1.0" height="1.0" />'
				print >> svgout, "</svg>";
			else:
				line=line.replace("font-size:10.00;", "font-size:10px;")
				print >> svgout, line

			if ln==9:
				print >> svgout, '<script type="application/ecmascript" xlink:href="/ode_svg.js"></script>'

			ln+=1
		svgout.close()
		svgin.close()
		os.system("mv %s.tmp %s" % (svgfn,svgfn))

		if permuN>0:
			perms={'N': permuN}

			self.update_progress("Permuting Trees...")
			args = ("%s/TOOLBOX/bicliquer/bicliquer" % (TOOL_DIR), '%s.odemat' % output_prefix, "-n%s" %(permuN), "-l%s"%(permuLimit) )
			proc = subprocess.Popen(args, stdout=subprocess.PIPE)

			getscores=False
			cutoff_set=False
			fulltime=0

			for line in proc.stdout:
				if line[:6]=='score:':
					getscores=True
				elif not getscores:
					m = re.search(r'Time limit exceeded at n=(.*)$', line)
					if m:
						permuN=m.group(1).strip()
						self.update_progress("	%s minute time limit exceeded. N=%s" % (permuLimit, permuN))
						self.update_progress("	Full results would need %s longer." % (fulltime))

					m = re.search(r'([^ ]+) ETA: (.*)$', line)
					if m:
						if m.group(1)=='cutoff':
							cutoff_set=True
							self.update_progress('	ETA: %s' %(m.group(2).strip()))

						elif cutoff_set:
							fulltime=m.group(2).strip()
						else:
							self.update_progress('	ETA: %s' %(m.group(2).strip()))

				elif line!='':
					m = re.search("^([^:]*):\t(.*)\t(.*)\t(.*)$",line)
					if m:
						perms[ m.group(1).strip() ] = {}
						perms[ m.group(1).strip() ]['value'] = m.group(2).strip()
						perms[ m.group(1).strip() ]['pvalue_low'] = m.group(3).strip()
						perms[ m.group(1).strip() ]['pvalue_high'] = m.group(4).strip()

			self._results['perms']=perms

def NewTool(*args, **kwargs):
	return PhenomeMap(*args, **kwargs)

if __name__ == '__main__':
	PhenomeMap().main()
