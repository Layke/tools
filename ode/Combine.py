#!/usr/bin/python
# USAGE: Combine.py input.odemat input.odepwd < parameters_json.txt > output_json.txt 2>status.txt

import re
import math 

from tools import Tool

class Combine(Tool):
	def __init__(self, *args, **kwargs): 
		super(Combine,self).__init__(*args, **kwargs)
		self.urlroot=''
		
	#####################################
	def run(self):	
		pass

def NewTool(*args, **kwargs):
	return Combine(*args, **kwargs)

if __name__ == '__main__':
	Combine().main()
